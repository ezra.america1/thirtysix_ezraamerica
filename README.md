# ThirtySix_EzraAmerica

Assignment code for new starter Ezra America

## Name
ThirtySix.

## Description
A technology familiarization assignment given to new starters at ByteOrbit.

The assignment requests the delivery of a web-based album directory application, written in the Django application.

## Installation
[DEPENDENCIES]
- OS: MacOS Monterey version 12.3.1
- git; git clone
- Access to the GitLab ByteOrbit project


[INSTRUCTIONS TO INSTALL]
Clone repository to your local machine.
```
git clone https://gitlab.com/ezra.america1/thirtysix_ezraamerica.git
```
Switch to branch thirtysix.
```
git checkout thirtysix
```
Run bash installation script.
```
    cd ./Docker_prod_config/;
    ## To install Docker
    ./deploy-prod.sh -d
    ## To install HomeBrew. NB. Homebrew will only be installed if the -d option is set.
    ./deploy-prod.sh -i
    ## To install the web-app build
    ./deploy-prod.sh -b
    ## To run complete setup (i.e., install docker, homebrew and build)
    ./deploy-prod.sh -dib
```
At this point the docker containers should be running.
    - The web app can be accessed via your local browser and navigating to localhost
    - The admin page can be viewed at localhost/admin and you may request the credentials from the author.

## Support
To contact the author, feel free to send an email to ezra.america@byteorbit.com

## License
Copyright 2022 Ezra America

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Project status
Complete
